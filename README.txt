********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Bounced Email Module
Author: Robert Castelo
Drupal: 6.x
********************************************************************
DESCRIPTION:

Handles email sent by the site which has bounced back.

Component module which provides service to other modules, don't install
this unless another module requires it.


********************************************************************
INSTALLATION:

Note: It is assumed that you have Drupal up and running.  Be sure to
check the Drupal web site if you need assistance.  If you run into
problems, you should always read the INSTALL.txt that comes with the
Drupal package and read the online documentation.

1. Place the entire directory of this module into your Drupal directory:
    sites/all/modules/

2. Enable the module by navigating to:
    administer > build > modules

Click the 'Save configuration' button at the bottom to commit your
changes. 


********************************************************************
AUTHOR CONTACT

- Report Bugs/Request Features:
   http://drupal.org/project/bounced_email
   
- Comission New Features:
   http://www.codepositive.com/contact
   
- Want To Say Thank You:
   http://www.amazon.com/gp/registry/O6JKRQEQ774F

        
********************************************************************
ACKNOWLEDGEMENT

Developed by Robert Castelo for Code Positive <http://www.codepositive.com>

